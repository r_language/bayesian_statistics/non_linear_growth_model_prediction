# Prediction_total_cases_non_lineary_growth_model



In the R file provided there is a code to predict the total number of cases with a non lineary growth model (seq, plot, nlsLM, SSgompertz, AIC)



Reference used for this code : A Bayesian analysis of the total number of cases of the COVID 19 when only a few data is available. A case study in the state of Goias, Brazil. Renato Rodrigues Silva, Wisley Donizetti Velasco, Wanderson daSilva Marques, Carlos Augusto Goncalves Tibirica


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021
